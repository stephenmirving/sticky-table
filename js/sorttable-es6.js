/**
 * @file sorttable-es6.js
 * @fileoverview Link this file at the bottom of the body of any HTML page that
 * contains one or more tables to make those tables sortable. Works along with
 * styles found in css/main.css to apply visual indication for the sorting.
 * This is the ECMAScript 6 version.
 * @version 1.1.6 08/15/19
 * @author Stephen M Irving
**/

/**
 * @function activateTableSorting
 * @description Immediately invoked function expression that implements sorting
 * on any tables found in the markup where a script tag links this file.
 * @public @global
 *
 * @param {object} [n=document] - The root element being searched for tabkes
 * to enable sorting on. When nothing is passed, n is a pointer to document.
 * @param {string} [q=n.getElementsByTagName('table')] - The class name for the
 * table(s) having sorting applied. When nothing is passed, q is a pointer to
 * all <table> elements within n. When a string is passed, that string is used
 * as a class name and n is searched for tables with that class applied.
 */
/**
 * Activates sorting functionality on all table elements of a given class name.
 * The user can click on a table's header cell once to sort the table according
 * to the ascending order or that cell's column. Clicking the same header cell
 * again will reverse the sort into descending order. Clicking a third time
 * returns the table to its original order. The function sorting a variety of
 * data types, including integers, floats, strings, currency, percentages, and
 * dates in the standardized formats accepted by the Date.parse() API. The
 * implementations works in in conjunction with the following CSS classes which
 * are applied by the function: '.table-sort-header', 'table-sort-asc', and
 * '.table-sort-desc', which are applied to the th elements that are children of
 * the thead element and provide a visual indication for the sorting feature
 * itself and the current sorting order (ascending, descending, or unsorted).
 *
 * @param {string} tables - The class name for the table(s) having the
 * sorting feature applied.
 * @param {string} [docRoot=document] - The optional document root ID that
 * contains the tables being sorted.
 */
const activateTableSorting = (tables, docRoot) => {
  'use strict';

  const l = parseFloat,
        m = u(/^(?:\s*)([\-+]?(?:\d+)(?:,\d{3})*)(\.\d*)?$/g, /,/g),
        g = u(/^(?:\s*)([\-+]?(?:\d+)(?:\.\d{3})*)(,\d*)?$/g, /\./g),
        getLen = x => x.length,
        foundTrue = (n, r) => n.map(r).indexOf(!0) != -1,
        t = (n, t) => {
          if (n) {
            for (let i = 0, a = getLen(n); i < a; ++i) {
              t(n[i], i);
            }
          }
        },
        v = (n, r) => {
          r(n),
          t(n.childNodes, (n) => {
            v(n, r);
          });
        },
        s = (n) => {
          const i = (n) => {
            const t = l(n);

            return !isNaN(t) && getLen(`${t}`) + 1 >= getLen(n) ? t : NaN;
          };

          let e = [];

          return t([i, m, g], (t) => {
            let a;

            getLen(e) || foundTrue((a = n.map(t)), isNaN) || (e = a);
          }), e;
        },
        activateSorting = (table) => {
          if (table.nodeName === 'TABLE') {
            for (
              var e = d(table),
                  a = e[0],
                  o = e[1],
                  u = getLen(a),
                  i = u > 1 && (getLen(a[0]) < getLen(a[1]) ? 1 : 0),
                  s = i + 1,
                  v = a[i],
                  p = getLen(v),
                  l = [],
                  m = [],
                  g = [],
                  h = s;
              h < u;
              ++h
            ) {
              for (let index = 0; index < p; ++index) {
                const T = a[h][index],
                      C = T.textContent || '';

                getLen(m) < p && m.push([]);
                m[index].push(C.trim());
              }
              g.push(h - s);
            }

            const classNameSortAsc = 'table-sort-asc',
                  classNameSortDesc = 'table-sort-desc',
                  b = () => {
                    let n = p;
                    while (n--) {
                      const headerClasses = v[n].classList;

                      headerClasses.remove(classNameSortAsc),
                      headerClasses.remove(classNameSortDesc),
                      (l[n] = 0);
                    }
                  };

            t(v, (headerCell, t) => {
              const headerClasses = headerCell.classList;

              l[t] = 0;

              headerClasses.add('table-sort-header'),
              headerCell.title = 'Click to sort',
              headerCell.addEventListener('click', () => {
                const i = m[t],
                      p = g.slice(),
                      n = (k, r) => { // A sort-compare function
                        const t = d[k],
                              e = d[r];

                        return (t > e ? a : e > t ? -a : a * (k - r));
                      },
                      f = (n) => {
                        const r = n.map(Date.parse);

                        return foundTrue(r, isNaN) ? [] : r;
                      };

                let a = l[t],
                    d = c(i),
                    v = (n, r) => a * i[n].localeCompare(i[r]) || a * (n - r), // Sort-compare function
                    parentEl = null;

                b(),
                a = (a == 1 ? -1 : +!a),
                a && headerClasses.add(
                  a > 0
                    ? classNameSortAsc
                    : classNameSortDesc
                ),
                (l[t] = a);

                (getLen(d) || getLen((d = f(i)))) && (v = n);

                p.sort(v);

                for (let nodeIndex = s; nodeIndex < u; ++nodeIndex) {
                  (parentEl = o[nodeIndex].parentNode),
                  parentEl.removeChild(o[nodeIndex]);
                }
                for (let nodeIndex = s; nodeIndex < u; ++nodeIndex) {
                  parentEl.appendChild(o[s + p[nodeIndex - s]]);
                }
              });
            });
          }
        },
        c = (n) => {
          let z = s(n);

          if (!getLen(z)) {
            const a = (n) => {
                    let e = n[0];

                    return t(n, (n) => {
                      while (!n.startsWith(e)) e = e.substring(0, getLen(e) - 1);
                    }), getLen(e);
                  },
                  o = a(n),
                  u = a(n.map(str => str.split('').reverse().join(''))),
                  i = n.map(n => n.substring(o, getLen(n) - u));
            z = s(i);
          }
          return z;
        },
        d = (n) => {
          const t = [],
                e = [];

          let r;

          return v(n, (node) => {
            const curNodeName = node.nodeName,
                  parentNodeName = node.parentNode.nodeName;

            if (parentNodeName !== 'TFOOT') {
              (curNodeName === 'TR'
                ? ((r = []), t.push(r), e.push(node))
                : ((curNodeName === 'TD' || curNodeName === 'TH') && r.push(node))
              );
            }
          }), [t, e];
        };

  function u(n, r) {
    return (t) => {
      let e = '';

      return (
        t.replace(n, (n, t, a) =>
          (e = t.replace(r, '') + '.' + (a || '').substring(1))), l(e)
      );
    };
  }

  docRoot = (typeof docRoot !== 'undefined'
    ? document.getElementById(docRoot)
    : document) || document;

  tables = docRoot.getElementsByClassName(tables);

  for (let tableNum = getLen(tables); tableNum--;) {
    try {
      activateSorting(tables[tableNum]);
    } catch (e) {}
  }
};
