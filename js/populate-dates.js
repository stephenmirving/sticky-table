((document) => {
  'use strict';

  const DATE_CELLS = document.querySelectorAll('tbody td:nth-child(5)'),
        DATE_FORMAT = {
          hourCycle: 'h24',
          year: 'numeric',
          month: '2-digit',
          day: '2-digit',
          hour: '2-digit',
          minute: '2-digit',
          second: '2-digit'
        };

  // Generates and formats a random date for each table cell in the Dates
  // column. Each date is between midnight Jan 01 1993 and midnight Jan 01 2019.
  // The parameters for the getRandomInt are UTC times representing those dates.
  DATE_CELLS.forEach( (cell) => {
    const dateParts = new Intl
      .DateTimeFormat('en-US', DATE_FORMAT)
      .formatToParts(new Date(getRandomInt(725864400000, 1546318800000)));

    cell.textContent =
      `${dateParts[4].value}-${dateParts[0].value}-${dateParts[2].value} ` +
      `${dateParts[6].value}:${dateParts[8].value}:${dateParts[10].value}`;
  });

  // The minimum is inclusive and the maximum is exclusive
  function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);

    return Math.floor(Math.random() * (max - min)) + min;
  }
})(document);
