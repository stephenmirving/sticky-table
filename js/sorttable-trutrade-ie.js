function activateTableSorting(tables, docRoot) {
  'use strict';

  var l = parseFloat,
      m = u(/^(?:\s*)([\-+]?(?:\d+)(?:,\d{3})*)(\.\d*)?$/g, /,/g),
      g = u(/^(?:\s*)([\-+]?(?:\d+)(?:\.\d{3})*)(,\d*)?$/g, /\./g),
      getLen = function (x) {
        return x.length;
      },
      foundTrue = function (n, r) {
        return n.map(r).indexOf(!0) != -1;
      },
      tableNum;

  function t(n, t) {
    if (n) {
      for (var i = 0, a = getLen(n); i < a; ++i) {
        t(n[i], i);
      }
    }
  }

  function u(n, r) {
    return function (t) {
      var e = '';

      return (
        t.replace(n, function (n, t, a) {
          return (e = t.replace(r, '') + '.' + (a || '').substring(1));
        }), l(e)
      );
    };
  }

  function s(n) {
    var e = [],
        i = function (n) {
          var t = l(n);

          return !isNaN(t) && getLen(t + '') + 1 >= getLen(n) ? t : NaN;
        };

    return t([i, m, g], function (t) {
      var a;

      getLen(e) || foundTrue((a = n.map(t)), isNaN) || (e = a);
    }), e;
  }

  function c(n) {
    var z = s(n),
        a = function (n) {
          var e = n[0],
              startsWith = function (str, search) {
                return str.substring(0, search.length) === search;
              };

          return t(n, function (n) {
            while (!startsWith(n, e)) {
              e = e.substring(0, getLen(e) - 1);
            }
          }), getLen(e);
        },
        o = a(n),
        u = a(n.map(function (str) {
          return str.split('').reverse().join('');
        })),
        i = n.map(function (n) {
          return n.substring(o, getLen(n) - u);
        });

    return (!getLen(z) ? s(i) : z);
  }

  function v(n, r) {
    r(n),
    t(n.childNodes, function (n) {
      v(n, r);
    });
  }

  function d(n) {
    var t = [],
        e = [],
        r;

    return v(n, function (node) {
      var curNodeName = node.nodeName,
          parentNodeName = node.parentNode.nodeName;

      if (parentNodeName !== 'TFOOT') {
        (curNodeName === 'TR'
          ? ((r = []), t.push(r), e.push(node))
          : ((curNodeName === 'TD' || curNodeName === 'TH') && r.push(node))
        );
      }
    }), [t, e];
  }

  function activateSorting(table) {
    if (table.nodeName === 'TABLE') {
      for (
        var e = d(table),
            a = e[0],
            o = e[1],
            u = getLen(a),
            i = u > 1 && (getLen(a[0]) < getLen(a[1]) ? 1 : 0),
            s = i + 1,
            v = a[i],
            p = getLen(v),
            l = [],
            m = [],
            g = [],
            classNameSortAsc = 'table-sort-asc',
            classNameSortDesc = 'table-sort-desc',
            h = s;
        h < u;
        ++h
      ) {
        for (var index = 0, T, C; index < p; ++index) {
          T = a[h][index],
          C = T.textContent || T.innerText || '';

          getLen(m) < p && m.push([]);
          m[index].push(C.trim());
        }
        g.push(h - s);
      }

      var b = function () {
        var n = p,
            headerClasses;

        while (n--) {
          headerClasses = v[n].classList;

          headerClasses.remove(classNameSortAsc),
          headerClasses.remove(classNameSortDesc),
          (l[n] = 0);
        }
      };

      t(v, function (headerCell, t) {
        var headerClasses = headerCell.classList;

        l[t] = 0;

        headerClasses.add('table-sort-header'),
        headerCell.title = 'Click to sort',
        headerCell.addEventListener('click', function () {
          var a = l[t],
              i = m[t],
              d = c(i),
              v = function (n, r) { // A sort-compare function
                return a * i[n].localeCompare(i[r]) || a * (n - r);
              },
              n = function (k, r) { // Other possible sort-compare function
                var t = d[k],
                    e = d[r];

                return (t > e ? a : e > t ? -a : a * (k - r));
              },
              f = function (n) {
                var r = n.map(Date.parse);

                return foundTrue(r, isNaN) ? [] : r;
              },
              parentEl = null,
              p = g.slice(),
              nodeIndex = s;

          b(),
          a = (a == 1 ? -1 : +!a),
          a && headerClasses.add(a > 0 ? classNameSortAsc : classNameSortDesc),
          (l[t] = a);

          (getLen(d) || getLen((d = f(i)))) && (v = n);

          p.sort(v);

          for (; nodeIndex < u; ++nodeIndex) {
            (parentEl = o[nodeIndex].parentNode),
            parentEl.removeChild(o[nodeIndex]);
          }
          for (nodeIndex = s; nodeIndex < u; ++nodeIndex) {
            parentEl.appendChild(o[s + p[nodeIndex - s]]);
          }
        });
      });
    }
  }

  docRoot = (typeof docRoot !== 'undefined'
    ? document.getElementById(docRoot)
    : document) || document;

  tables = docRoot.getElementsByClassName(tables);

  for (tableNum = getLen(tables); tableNum--;) {
    try {
      activateSorting(tables[tableNum]);
    } catch (e) {}
  }
}
