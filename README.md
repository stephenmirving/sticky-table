# sticky-table

## A commented explanation for implementing stylized and sortable tables

### Using these files

To view this implementation, simply open the index.html file in a browser of your choice.
For details about each part of the implementation, view the comments in the following files:

* index.html
* css/main.css
* js/sorttable.js

### Production files list

* index.html | The table markup

* css/main.css | The uncompressed stylesheet for the table

* css/main.min.css | The minified/compressed stylesheet

* css/main.min.css.map | Maps the compressed styles to the uncompressed styles
for in-browser debugging

* js/classlist-polyfill.js | A cross-browser polyfill for the element.classlist
feature. This script must be linked before the sorttable.min.js file for the latter
to work in older browsers.

* js/sorttable.js | The uncompressed table-sorting script

* js/sorttable.min.js | The minified/compressed table-sorting script. This file
must be linked at the bottom of the html body tag (right above closing body tag)
to make the tables in the markup sortable.

* js/sorttable.min.js.map | Maps the compressed/minified script to the uncompressed
script for in-browser debugging.

### Development files list (not needed for production implementation)

* package.json | Details on repository and packages used
* package-lock.json | Detailed info on packages used
* node_modules/* | The node modules used in development
* .gitignore | Git ignore file
* .eslintrc | ESLint (Javascript linter) configuration file
* .htmlhint | HTML-Hint (HTML linter) configuration file
