#! /usr/bin/perl

use DBI;
require "db.config";
use CGI qw/:standard/;

print "Content-type: text/html\n\n";

# read in variables
$orderby = &param(orderby);
# $start_date = &param(start_date);
# $end_date = &param(end_date);
$sstatus = &param(sstatus);
$id = &param(id);

$dbh = DBI->connect($dbname,$dbuser,$dbpassword,$dbd);
if (!$dbh) {
	print "<script>alert('There seems to have been an error connecting to the database!');</script>\n<!-- $DBI::errstr -->\n";
	die "There was an error connecting to $dbname. $DBI::errstr";
}

# repeat this for all fields to be searched on
# if ($start_date) {
# $start_date = $dbh->quote($start_date);
# push(@where, "datetime >= $start_date");
# }
#	else {
#		push(@where, "datetime >= '1900-01-01' ");
#	}

# if ($end_date) {
# $end_date = $dbh->quote($end_date);
# push(@where, "datetime <= $end_date");
# }
# else {
#		push(@where, "datetime <= '2100-01-01' ");
# }

# if ($sstatus eq 'open') {
# push(@where2, "sstatus = 'new' ");
# push(@where2, "sstatus = 'open' ");
# push(@where2, "sstatus = 'in progress' ");
# push(@where2, "sstatus = 'need more info' ");
# }

# if ($sstatus eq 'all') {
#		push(@where2, "sstatus = 'new' ");
#		push(@where2, "sstatus = 'open' ");
#		push(@where2, "sstatus = 'in progress' ");
#		push(@where2, "sstatus = 'completed - closed' ");
#		push(@where2, "sstatus = 'canceled - closed' ");
#		push(@where2, "sstatus = 'become PFI - closed' ");
#		push(@where2, "sstatus = 'need more info' ");
# }

if ($id) {
	$id = $dbh->quote($id);
	push(@where, "ID = $id");
}

if ($orderby) {
	$orderby = $orderby;
}
else {
	$orderby = 'datetime';
}

# create the SQL syntax for the where clause
$where = join(" AND ", @where);
$where = "WHERE $where" if $where;

# create and run the query
$query = "SELECT * FROM fire_alarm $where ORDER BY $orderby";
# print "\n$query";

$sth = $dbh->prepare($query);
if (!$sth->execute()) {
	print "<br>There seems to have been an error with the SQL statement!<br>\n";
	print "<!-- $DBI::errstr -->\n";
	die "There was an error with the sql statement. $DBI::errstr";
}

# print the top of the HTML
print << "E_O_F";
<!doctype html>
<html lang="en-US">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="mobile-web-app-capable" content="yes">

	<title>Fire Alarm Incident Database</title>

	<meta name="keywords" content="MUSC, hospital, fire, alarm, database, incident, report">
	<meta name="description" content="A site for viewing fire alarm incidents and generating new incident reports.">
	<meta name="author" content="Michael G Irving">
	<meta name="creation_date" content="2009-11">
	<meta name="language" content="EN">

	<!-- <link rel="stylesheet" href="new_musc2/brand.css"> -->
	<link rel="stylesheet" href="css/stickytable.css">
	<!--[if lt IE 9]>
		<link rel="stylesheet" href="css/stickytable.css">
	<![endif]-->
</head>

<body>
<main role="main">
	<div class="table-container">
		<table class="sticky-table">
			<caption>
				<h1 class="table-title">Fire Alarm Incident Database</h1>
				<p class="style1 home-link"><a href="../formsToolbox/facilities/fire_alarm_incident/">Fire Alarm Incident Home</a></p>
			</caption>
			<colgroup>
				<col class="table-col1">
				<col class="table-col2">
				<col class="table-col3">
				<col class="table-col4">
				<col class="table-col5">
				<col class="table-col6">
				<col class="table-col7">
				<col class="table-col8">
				<col class="table-col9">
				<col class="table-col10">
			</colgroup>
			<thead class="table-head" title="Click to sort">
				<tr class="table-row">
					<th class="col-head" scope="col">Inc<br>No.</th>
					<th class="col-head" scope="col">Date</th>
					<th class="col-head" scope="col">Sub By</th>
					<th class="col-head" scope="col">Building</th>
					<th class="col-head" scope="col">Floor</th>
					<th class="col-head" scope="col">Room</th>
					<th class="col-head" scope="col">Activation Device</th>
					<th class="col-head" scope="col">Cause</th>
					<th class="col-head" scope="col">Activated By</th>
					<th class="col-head" scope="col">EDIT</th>
				</tr>
			</thead>
			<tbody class="table-body">
E_O_F

# Loop through the query results

while (@results = $sth->fetchrow) {
	my ($ID, $datetime, $netid, $sstatus, $sub_name, $sub_phone1, $sub_phone2,
			$sub_pager, $date, $time_act, $time_clear, $building, $flor, $other_bldg,
			$room_no, $activation_device, $cause_of_alarm, $activated_by, $ona, $asif,
			$arafd, $fdr, $cror, $crp, $acp, $detailed_summary, $findings,
			$corrective_actions
	) = @results;

# my $deficiency = substr $deficiency, 0, 50;

# print outthe HTML table with the data row by row

	print << "E_O_F";

		<tr class="table-row">
			<td class="cell">$ID</td>
			<td class="cell">$datetime</td>
			<td class="cell">$sub_name</td>
			<td class="cell">$building $other_bldg</td>
			<td class="cell">$flor</td>
			<td class="cell">$room_no</td>
			<td class="cell">$activation_device</td>
			<td class="cell">$cause_of_alarm</td>
			<td class="cell">$activated_by</td>
			<td class="cell">
				<a class="alarm-edit-link" href="fire_alarm_edit.cgi?id=$ID">edit</a>
			</td>
		</tr>

E_O_F
}

# print out the bottom of the html
print << "E_O_F";
				</tbody>
				<tfoot class="table-foot">
					<tr class="table-row">
						<td class="attribution" colspan="13">Written by Michael Irving - November 2009</td>
					</tr>
					<tr class="table-row">
						<td class="style9 footer-caption-primary" colspan="13">Fire Safety Incident Database</td>
					</tr>
					<tr class="table-row">
						<td class="footer-caption-sub" colspan="13">MUSC Medical Center Intranet</td>
					</tr>
				</tfoot>
			</table>
		</div> <!-- End of .table-container -->
	</main>

	<script src="js/classlist-polyfill.min.js"></script>
	<script src="js/sorttable.min.js" defer></script>
</body>
</html>

E_O_F

exit;
